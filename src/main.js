import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import sudoku from "@/components/Sudoku";
import MainMenu from "@/components/MainMenu";
import NewGameSettings from "@/components/NewGameSettings";
import Score from "@/components/Score";

Vue.use(Router);

const router = new Router({
  routes: [
    {
      name: 'sudoku',
      props: (route) => ({
        ...route.params
      }),
      path: '/sudoku',
      component: sudoku,
    },{
     path:"/",
      component: MainMenu,
    },
    {
      path:"/setting",
      component: NewGameSettings,
    },
    {
      path:"/score",
      component: Score,
    }
  ]
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
});
