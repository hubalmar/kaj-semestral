import Square from "@/javascript/Squares/Square";

export default class Wrong extends Square {
    constructor(x, y) {
        super(x, y);

    }

    class = "Wrong";
    wrongNeighbours = [];

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/im/" + "wrong.jpg");
        return img;
    }();

    getImage() {
        return Wrong.image;
    }
}