import Wrong from "@/javascript/Squares/Wrong";

export default class SelectedWrong extends Wrong {
    constructor(x, y) {
        super(x, y);
    }

    class = "SelectedWrong";
    static image = function() {
        let img = new Image();
        img.src = require("../../assets/im/" + "wrong2.jpg");
        return img;
    }();
    getImage() {
        return SelectedWrong.image
    }
}