import Square from "@/javascript/Squares/Square";

export default class SelectedSquare extends Square {
    constructor(x, y) {
        super(x, y);
    }
    class = "SelectedSquare";
    static image = function() {
        let img = new Image();
        img.src = require("../../assets/im/" + "known.jpg");
        return img;
    }();

    getImage() {
        return SelectedSquare.image
    }
}