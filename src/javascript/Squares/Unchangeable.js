import Square from "@/javascript/Squares/Square";

export default class Unchangeable extends Square {

    //Square that are generated in the generated sudoku so they can't be changed by user
    constructor(x, y) {
        super(x, y);
    }

    class = "Unchangeable";
    changeable = false;

    static image = function() {
        let img = new Image();
        img.src = require("../../assets/im/" + "unchangeable.jpg");
        return img;
    }();

    getImage() {
        return Unchangeable.image
    }
}