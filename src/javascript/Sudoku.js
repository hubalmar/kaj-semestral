import Square from "@/javascript/Squares/Square";
import Unchangeable from "@/javascript/Squares/Unchangeable";

export default class Sudoku {

    constructor() {
        this.sudoku = [[]];
        this.time = new Date().getTime();
        this.solution = [[]]
    }

    /**
     * generates random sudoku
     **/
    generateSudoku(numberOfSquares){
        let randomGrid = [[]];
        for(let i = 0; i < 9; i++){
            this.sudoku[i] = new Array(9);
            this.solution[i] = [0,0,0,0,0,0,0,0,0];
            randomGrid[i] = [0,0,0,0,0,0,0,0,0];
            for(let j = 0; j < 9; j++){
                this.sudoku[i][j] = new Square(i, j);
            }
        }

        this.solve(0,0,randomGrid,0);
        let boardForUser = this.createSudokuAlgorithm(numberOfSquares);
        for(let i = 0; i < 9; ++i){
            for(let j = 0; j < 9; ++j) {
                if(boardForUser[i][j]) {
                    let unchangeable = new Unchangeable(i, j);
                    unchangeable.number = boardForUser[i][j];
                    this.sudoku[i][j] = unchangeable;
                }
            }
        }
    }


    /**
     * loads sudoku from localStorage and solves it
     */
    continueSolve(){
        let f = [[]];
        for(let i = 0; i < 9; i++){
            f[i] = new Array(9);
            for(let j=0; j< 9; ++j){
                f[i][j] = this.sudoku[i][j].number;
            }
        }
        this.solveSuduku(f);

    }

    /**
     * deletes squares from solved sudoku and checks if it has more than 1 solution, if it has only 1 solution, the sudoku is generated
     * @param numberOfSquares how many squares should be deleted from solution
     * @returns {*[][]}
     */
    createSudokuAlgorithm(numberOfSquares){
        let sudoku = [[]];
        let order = new Array(81);
        for(let i = 0; i < 81; ++i){
            order[i] = i;
        }
        do {
            this.shuffleArray(order);
            for(let i = 0; i < 9; i++){
                sudoku[i] = Array.from(this.solution[i]);
            }
            for (let i = 0; i < numberOfSquares; ++i) {
                let nmb = order[i];
                sudoku[Math.floor(nmb / 9)][nmb % 9] = 0;
            }
        }while(this.solve(0, 0, sudoku, 0) !== 1)
        return sudoku;
    }

    /**
     * Algorithm that solves sudoku from given grid
     */
    solve(row, col, grid, count)
    {
        if (row === 9) {
            row = 0;
            if (++col === 9)
                return 1+count;
        }
        if (grid[row][col] !== 0)
            return this.solve(row+1,col,grid, count);

        let b = [1,2,3,4,5,6,7,8,9];
        this.shuffleArray(b);
        for (let i = 0; i <= 8 && count < 2; ++i) {
            let val = b[i];
            if (this.isSafe(grid,row,col,val)) {
                grid[row][col] = val;
                if(count === 0) {
                    this.solution[row][col] = val;
                }
                count = this.solve(row+1,col,grid, count);
    }
    }
        grid[row][col] = 0;
        return count;
    }

    shuffleArray(array) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }
    solveSuduku(grid){
        for(let i = 0; i < 9; i++){
            this.solution[i] = Array.from(grid[i]);
        }
        this.solve(0,0,grid,0)
    }

    /**
     *  checks if the number can be put to the square (not already in row, column or square)
     */
    isSafe(grid, row, col, number)
    {
        for(let x = 0; x <= 8; x++)
            if (grid[row][x] === number)
                return false;

        for(let x = 0; x <= 8; x++)
            if (grid[x][col] === number)
                return false;
        let startRow = row - row % 3,
            startCol = col - col % 3;

        for(let i = 0; i < 3; i++)
            for(let j = 0; j < 3; j++)
                if (grid[i + startRow][j + startCol] === number)
                    return false;

        return true;
    }
}